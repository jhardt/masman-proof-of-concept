﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Masman_Proof_of_Concept.Startup))]
namespace Masman_Proof_of_Concept
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
