﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ActiveDirectoryClient;

namespace Masman_Proof_of_Concept.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            return View(ActiveDirectoryLib.FindAllUsers());
        }

        // POST: ChangePassword
        [HttpPost]
        public ActionResult ChangePassword(FormCollection collection)
        {
            if (collection["sAMAccountName"] != null && collection["password"] != null)
            {
                ActiveDirectoryLib.ResetUserPassword(collection["sAMAccountName"], collection["password"]);
            }
            return RedirectToAction("Index");
        }

        // POST: ValidateCredentials
        [HttpPost]
        public ActionResult ValidateCredentials(FormCollection collection)
        {
            if (collection["sAMAccountName"] != null && collection["password"] != null)
            {
                return Content(ActiveDirectoryLib.ValidateCredentials(collection["sAMAccountName"], collection["password"]).ToString());
            }
            return RedirectToAction("Index");
        }

        // GET: Users/Details/5
        // public ActionResult Details(int id)
        // {
        //     return View();
        // }

        // GET: Users/Create
        // public ActionResult Create()
        // {
        //     return View();
        // }

        // POST: Users/Create
        // [HttpPost]
        // public ActionResult Create(FormCollection collection)
        // {
        //     try
        //     {
        //         // TODO: Add insert logic here
        // 
        //         return RedirectToAction("Index");
        //     }
        //     catch
        //     {
        //         return View();
        //     }
        // }

        // GET: Users/Edit/5
        // public ActionResult Edit(int id)
        // {
        //     return View();
        // }

        // POST: Users/Edit/5
       // [HttpPost]
       // public ActionResult Edit(int id, FormCollection collection)
       // {
       //     try
       //     {
       //         // TODO: Add update logic here
       // 
       //         return RedirectToAction("Index");
       //     }
       //     catch
       //     {
       //         return View();
       //     }
       // }

        // GET: Users/Delete/5
        // public ActionResult Delete(int id)
        // {
        //     return View();
        // }

        // POST: Users/Delete/5
        // [HttpPost]
        // public ActionResult Delete(int id, FormCollection collection)
        // {
        //     try
        //     {
        //         // TODO: Add delete logic here
        // 
        //         return RedirectToAction("Index");
        //     }
        //     catch
        //     {
        //         return View();
        //     }
        // }
    }
}
