﻿using System.Web;
using System.Web.Mvc;

namespace Masman_Proof_of_Concept
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
