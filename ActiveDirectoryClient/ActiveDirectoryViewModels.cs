﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Masman_Proof_of_Concept.Models
{
    public class UserViewModel
    {
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string SAMAccountName { get; set; }
        public string Password { get; set; }
    }
}