﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Masman_Proof_of_Concept.Models;

namespace ActiveDirectoryClient
{
    public static class ActiveDirectoryLib
    {
        private static string Server = "192.168.0.10";
        private static string Username = "HARDT\\Administrator";
        private static string Password = "mso1234!";

        public static IEnumerable<UserViewModel> FindAllUsers()
        {
            Console.WriteLine("Hello World");

            using (var context = new PrincipalContext(ContextType.Domain, Server, Username, Password))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    foreach (UserPrincipal result in searcher.FindAll())
                    {
                        var user = new UserViewModel {
                            GivenName = result.GivenName,
                            Surname = result.Surname,
                            SAMAccountName = result.SamAccountName
                        };

                        yield return user;
                    }
                }
            }
        }

        public static bool ValidateCredentials(string sAMAccountName, string password)
        {
            using (PrincipalContext context = new PrincipalContext(ContextType.Domain, Server, Username, Password))
            {
                return context.ValidateCredentials(sAMAccountName, password);
            }
        }

        public static void ResetUserPassword(string sAMAccountName, string password)
        {
            using (var context = new PrincipalContext(ContextType.Domain, Server, Username, Password))
            {
                using (var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, sAMAccountName))
                {
                    user.SetPassword(password);
                }
            }
        }
    }
}
