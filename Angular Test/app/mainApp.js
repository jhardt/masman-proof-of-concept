﻿angular.module('mainApp', [])

.controller('TestCtrl', ['$scope', 'usersFactory', function($scope, usersFactory) {
	$scope.users = usersFactory;
}])

.controller('AddUserCtrl', ['$scope', 'usersFactory', function($scope, usersFactory) {
  $scope.user = new Object();

  $scope.addUser = function() {
    usersFactory.push($scope.user);
    $scope.user = new Object();
  };
}]);